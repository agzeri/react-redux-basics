# React Redux Basics

## Context

Learn how to integrate React and Redux to develop a powerful single page application.

## The Assignment

You will create a simple application using Redux as State Management.

### Setup Instructions

In Terminal:

```sh
# (1) Clone the repo
$ git clone https://gitlab.com/agzeri/react-redux-basics

# (2) Go to folder
$ cd react-redux-basics/starter-code

# (3) Install dependencies
$ npm install

# (4) Spin up the development server
$ npm start
```

## Deliverables

### Sprint 1 | Install Dependencies

**Summary**

In this step, install some dependencies, create a reducer, and create the Redux store.

**Instructions**

+ Install `redux`.
+ Open `/src/store.js`.
+ Import [`createStore`](https://redux.js.org/api/createstore) from Redux.
+ Create an empty initial state object.
+ Write a simple [reducer](https://redux.js.org/basics/reducers). It should return state by default.
+ [Create](https://redux.js.org/basics/store) and export a Redux store.

### Sprint 2 | Redux DevTools

**Summary**

You’ll install the Redux DevTools.

**Instructions**

+ Install the Redux DevTools extension
  + [Chrome page](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)
  + [Firefox page](https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools/)
+ Install `redux-devtools-extension` as a dev dependency.
+ [Configure your store](https://github.com/zalmoxisus/redux-devtools-extension#installation) with Redux DevTools.
+ If you import your store on `/src/components/Name/Name.js` you’ll see the Redux DevTools Panel active.

**Design**

![Redux DevTools Panel](/demos/s2.png)

### Sprint 3 | Name and Category

**Summary**

In this step you’ll create the first two cases to handle actions in the reducer.

**Instructions**

+ Add two properties to `initialState` in `store.js` (see Table 3.1).
  + One to store the recipe name.
  + One to store the recipe category.
+ Create two action type constants (see Table 3.2).
  + One for updating the recipe name.
  + One for updating the recipe category.
  + Remember to export them not as default.
+ Add two cases to the switch.
  + These should match the constants just created.
  + These should alter the appropiate part of the state object, and return a new value.

**Table 3.1, Initial State Properties**

|#|Name|Description|
|-|-|-|
|1|`name`|The recipe name|
|2|`category`|The recipe category|

**Table 3.2, Action Type**

|#|Name|Description|
|-|-|-|
|1|`UPDATE_NAME`|This constant should hold the "UPDATE_NAME" string.|
|2|`UPDATE_CATEGORY`|This constant should hold the "UPDATE_CATEGORY" string.|

### Sprint 4 | Name View

**Summary**

In this step we’ll set up our first view `Name.js`, to use the action types we just added to `store.js`

**Instructions**

+ Open `/src/components/Name/Name.js`.
+ Import the `store` and the name and category action types from `/src/store.js`. (Do it in one line).
+ Save the input value from the Recipe Name in the state.
  + Create an empty state with one string property (`name`).
  + Use the `onChange` event to trigger the function every time a user types.
  + Capture the value and save it in the state, in the same way use `value` in the input to show the saved value from state.
+ Save the select value from the Category in the state.
  + Add a new empty string property in State (`category`).
  + Use the `onChange` event to capture the value selected.
  + Save the value selected in the State and do the same with the `value` attribute from `select`.
+ Create a `saveChanges` function and attached to the `onClick` event from the button.
+ Inside `saveChanges` method, use the `dispatch` function of the `store` to send an action object.
  + The object should have a type that matches the name action type (`UPDATE_NAME`) that was imported.
  + The object should have a `payload` property that sends the value of the name input box. _Remember, where do you have stored that value?_
+ Add another `dispatch` inside `saveChanges` method for the category action type.
+ _To test the actions and make sure it’s working, you can inspect in the Redux DevTools Panel, or add a console to the `action` inside the reducer_.

**Design**

![Name view](/demos/s4.png)

### Step 5 | Keeping the values

**Summary**

At this point, your actions are performing and we can save in the State the input values from `Name.js` to Redux, but we aren’t using that data to keep our input boxes from clearing. If we hit the Next button, and then the Previous button, the input boxes still forget what we typed in. This is not an error, we are just not display them in the interface. They already exist in the Redux Store.

**Instructions**

+ Inside the `constructor`, use `getState` method from `store`.
  + Store the returned value in a `const` so we can reference it later.
+ Now change the initial state to use the appropriate values from Redux state instead of empty strings.
  + This means that when the component first mounts, it will pull the data we saved earlier.

**Design**

![Keeping the State](/demos/s5.mov)

### Step 6 | Implement Author View

**Summary**

In this step you are going to repeat all setup we did for `Name.js` for `Author.js`.

**Instructions**

+ Open `/src/store.js`
+ Add two properties to `initialState`.
  + One to store the author’s first name.
  + One to store the author’s last name.
+ Create and export two constants to match.
+ Add two corresponding cases to the switch.
+ Open `/src/components/Author/Author.js`.
+ Import the `store` and the two action types just created from `/src/store.js`.
+ Use the local state to save values from First Name and Last Name input boxes.
  + Implement the `onChange` event in both cases.
  + Pull data from the local state in the `value` attribute.
+ Create a `saveChanges` method and attach it to both button in the `onClick` event.
+ Inside `saveChanges` method, use `dispatch` (from `store`) twice, to send two separata action type objects.
  + The action objects should use the action types that were imported.
  + Don’t forget to send the data in `payload` property.
+ Inside the `constructor`, invoke `getState` from `store` and use the appropiate values from Redux store to initialize both properties.

**Design**

![Author View](/demos/s6.mov)

### Step 7 | Ingredients View

**Summary**

In this step, we’ll set up `Ingredients.js` much the same way we have `Name.js` and `Author.js`, but we’ll need to add one more piece. The `Ingredients.js` view will require some extra effort, because it needs to update Redux before we navigate to another page. So, we need to be able to pull data from Redux whenever there are changes, not just in the constructor as initial values.

**Instructions**

+ Open `/src/store.js`.
+ Add a property to `initialState` to store the **list** of ingredients.
+ Create and export a constant to match.
+ Add a `case` to the `switch`, this logic should append the ingredient to the list.
+ Open `/src/components/Ingredients/Ingredients.js`.
+ Import the `store` and the ingredients action type from `/src/store.js`.
+ Create a `addIngredient` method and attach it to the `onClick` event from button (the one that have the `+` icon).
+ Inside `addIngredient` method, use `dispatch` to send an action object.
  + It should use the action type that was imported.
  + It should pull the input data from local state for the `payload`.
+ Inside the `constructor`, invoke the `getState` method from `store`, and use the appropiate value from Redux state inside the component initial state.
+ Every time you click the `+` icon you don’t see anything. This is working well, the problem is that you are not updating the interface.
  + Inside `componentDidMount`, use the `subscribe` method from `store`.
  + `subscribe` takes a callback function as its argument.
  + This callback should invoke `getState` exactly like the `constructor` does.
  + Then, it should call `this.setState` and use the correct value from the Redux state to update the component’s state.

**Design**

![Ingredients View](/demos/s7.mov)

### Step 8 | Instructions View

**Summary**

In this step, we’ll set up `Instructions.js` like we have `Ingredients.js`.

**Instructions**

+ Because the process it’s exactly the same, this time there are no instructions.

**Design**

![Instructions View](/demos/s8.mov)

### Step 9 | Create a Recipe

**Summary**

In this step, we’ll use all the values we’ve saved in Redux to create a recipe.

**Instructions**

+ Open `/src/store.js`.
+ Add a property to `initialState` to store the **list** of recipes.
+ Create and export a constant to match.
+ Add a `case` to the `switch`.
  + This case should use the values already stored on state to create the new recipe and won’t rely on `payload`.
+ Open `/src/components/Instructions/Instructions.js`.
+ Import the recipe action type from `/src/store.js`.
+ Create a `addRecipe` method and attach it to the `onClick` event from “Create” button.
+ Inside `addRecipe`, use `dispatch` to send an action object.
  + It should use the action type that was imported.
  + It should **NOT** include a `payload`.

_To test it you can use `console.log` or check the Redux DevTools Panel_.

### Step 10 | Show all recipes

**Summary**

Finally we”ll get our recipes to display them in `Home.js`.

**Instructions**

+ Open `/src/components/Home/Home.js`.
+ The recipes are actually stored in Redux state, you need to import the store and use the `constructor` to access the initial values.

**Design**

![Recipes in Home](/demos/s10.mov)

### Step 11 | BONUS

**Summary**

Not mandatory, but helpful for better UX.

**Instructions**

+ When we create a recipe, the fields in our form don’t clear. Create another action type to clear the fields when we create a new recipe.
+ Right now each recipe displays a delete button that doesn’t do anything. Create another action type to delete a recipe.

**Design**

![Bonus Demo](/demos/s11.mov)
