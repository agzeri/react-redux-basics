import React, { Component } from "react";
import { HashRouter as Router } from "react-router-dom";
import routes from "./routes";
import "./App.css";

class App extends Component {
  render() {
    return (
      <Router>
        <header className="header">
          {routes}
        </header>
      </Router>
    );
  }
}

export default App;
