import React from "react";
import "./RecipeCard.css";

const RecipeCard = props => {
  const {
    name,
    category,
    authorFirst,
    authorLast,
    ingredients,
    instructions
  } = props;

  const ingredientsDisplay = ingredients.map((ingredient, i) => {
    return <li key={i}><i className="fa fa-check-square-o"/> {ingredient}</li>;
  });
  const instructionsDisplay = instructions.map((instruction, i) => {
    return <li key={i}><i className="fa fa-hashtag" /> {instruction}</li>;
  });
  
  return (
    <div className="card">
      <div className="grid grid--baseline grid--spaced card__header">
        <h2 className="card__title">{name}</h2>
        <p className="card__tag">#{category}</p>
      </div>
      <small>by {authorFirst} {authorLast}</small>
      <h3 className="card__subtitle">Ingredients</h3>
      <ul className="card__list">{ingredientsDisplay}</ul>
      <h3 className="card__subtitle">Instructions</h3>
      <ol className="card__list">{instructionsDisplay}</ol>
      <button className="card__close"><i className="fa fa-times"/></button>
    </div>
  );
};

export default RecipeCard;
