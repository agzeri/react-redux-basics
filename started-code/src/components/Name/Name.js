import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./Name.css";

class Name extends Component {  
  render() {
    return (
      <div className="form">
        <div className="form__group">
          <h2 className="question__title">Recipe Name</h2>
          <input
            className="question__input"
          />
        </div>
        <div>
          <h2 className="question__title">Category</h2>
          <select
            className="question__select"
          >
            <option value={""}>----</option>
            <option value={"Breakfast"}>Breakfast</option>
            <option value={"Second Breakfast"}>Second Breakfast</option>
            <option value={"Brunch"}>Brunch</option>
            <option value={"Lunch"}>Lunch</option>
            <option value={"Dinner"}>Dinner</option>
            <option value={"Drinks"}>Drinks</option>
            <option value={"Dessert"}>Dessert</option>
          </select>
        </div>
        <button className="button button__center">
          <Link to="/add/author">
            Next
          </Link>
        </button>
      </div>
    );
  }
}

export default Name;
