import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./Ingredients.css";

class Ingredients extends Component {
  constructor(props) {
    super(props);

    this.state = {
      input: ""
    };
  }
  
  saveIngredient = name => {
    this.setState({
      input: name
    });
  }

  render() {
    /*const ingredients = this.state.ingredients.map((ingredient, i) => {
      return <li key={i}>{ingredient}</li>;
    });*/
    
    return (
      <div className="h">
        <div className="form">
          <h2 className="question__title">Ingredients</h2>
          <div>
            {/*<ul>{ingredients}</ul>*/}
          </div>
          <div className="grid">
            <input
              className="question__input"
              value={this.state.input}
              onChange={e => this.saveIngredient(e.target.value)}
            />
            <button
              style={{ margin: "10px 0", border: 0, }}
            >
              <i className="fa fa-plus" />
            </button>
          </div>
          <div className="form__group--center">
            <Link to="/add/author">
              <button className="button">Prev</button>
            </Link>
            <Link to="/add/instructions">
              <button className="button">Next</button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Ingredients;
