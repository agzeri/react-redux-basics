import React, { Component } from "react";
import { Link } from "react-router-dom";

class Instructions extends Component {
  render() {

    /*const instructions = this.state.instructions.map((instruction, i) => {
      return <li key={i}>{instruction}</li>;
    });*/

    return (
      <div className="form">
        <h2 className="question__title">Instructions</h2>
        <div>
          {/*<ol>{instructions}</ol>*/}
        </div>
        <div className="grid">
          <input
            className="question__input"
          />
          <button
            style={{ margin: "10px 0", border: 0, }}
          >
            <i className="fa fa-plus" />
          </button>
        </div>

        <div className="form__group--center">
          <Link to="/add/ingredients">
            <button className='button'>Prev</button>
          </Link>
          <Link to="/">
            <button className='button'>Create</button>
          </Link>
        </div>
      </div>
    );
  }
}

export default Instructions;
