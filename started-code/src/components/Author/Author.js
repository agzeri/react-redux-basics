import React, { Component } from "react";
import { Link } from "react-router-dom";

import './Author.css';

class Author extends Component {
  render() {
    return (
      <div className="form">
        <div className="form__group">
          <h2 className="question__title">Author First Name</h2>
          <input
            className="question__input"
          />
        </div>
        <div>
          <h2 className="question__title">Author Last Name</h2>
          <input
            className="question__input"
          />
        </div>
        <div className="form__group--center">
          <button className="button">
            <Link to="/add/name">Prev</Link>
          </button>
          <button className="button">
            <Link to="/add/ingredients">Next</Link>
          </button>
        </div>
      </div>
    );
  }
}

export default Author;
